import {useNavigate,useParams} from "react-router-dom";
import {useState,useEffect} from "react";
import { PokemonDetails } from "../../types/types";
import PokeballImg from "../icons/pokeball.png";
//import BulbasaurImg from "../icons/bulbasaur.gif";
import Footer from "../components/Footer";
import styles from "./pokemon.module.css";
import { fetchPokemon } from "../../api/fetchPokemon";
import { waitFor,formatHeightSpanish,formatWeightSpanish,formatTypeSpanish } from "../../utils/utils";
import LoadingScreen from "../components/LoadingScreen";


const Pokemon = () => {

        //* Constantes.
        const [isLoading,setIsLoading] = useState(false); //? Parametro que iniciación para la la pantallade carga.
        //? Se crea un array de constantees una que recoge el esdao inicial y otra para el update.
        const [pokemon,setPokemon] = useState<PokemonDetails>(); //? useState es un Hook de React que te permite agregar una variable de estado a tu componente.
        //?hook useParams de React Router para acceder a los parámetros de la ruta. useParams devuelve un objeto con claves/valores con los parámetros dinámicos de la URL. 
        //?En este caso, se está extrayendo el valor del parámetro name de la URL y asignándolo a una variable local llamada name
        const {name} = useParams();
        const navigate = useNavigate(); //? Hook de React Route que devuelve una función que permite navegar de forma programática.

        //? Ejecutar funciones fuera del flujo de renderizado. Con useEffect podemos emular algunos métodos de ciclo de vida.
        useEffect( ()=>{
                
                async function getPokemon(){
                        setIsLoading(true);
                        await waitFor(500);
                        const fetchedPokemon = await fetchPokemon(name as string);
                        setPokemon(fetchedPokemon);
                        setIsLoading(false);
                }
                
                getPokemon();
                

        },[name]);

        
    //? Se crea la llamada a la pantalla de carga.
        if (isLoading || !pokemon) {
                return <LoadingScreen/>
        }
        return (
        <>
                <button 
                type="button"
                className={styles.pokemonButton}
                onClick={ () => navigate(-1)}
                >
                        <img 
                        src={PokeballImg} 
                        alt="Pokeball"
                        className={styles.pokeballImg}
                        /> Volver
                </button>
                <div className={styles.pokemon}>
                        <main className={styles.pokemonInfo}>
                                <div className={styles.pokemonTitle}>{name?.toUpperCase()}</div>
                                <div>Nº: {pokemon?.id}</div>
                                <div>
                                        <img className={styles.pokemonInfoImg} src={pokemon?.imgSrc} alt={pokemon?.name} />
                                </div>
                                <div>Vida: {pokemon?.hp}</div>
                                <div>Ataque: {pokemon?.attack}</div>
                                <div>Defensa: {pokemon?.defense}</div>
                                <div>Ataque Especial: {pokemon?.specialAttack}</div>
                                <div>Defensa Especial: {pokemon?.specialDefense}</div>
                                <div>Velocidad: {pokemon?.speed}</div>
                                <div>Tipo: {formatTypeSpanish(pokemon?.type)}</div>
                                <div>Altura: {formatHeightSpanish(pokemon?.height).toFixed(2)} m.</div>
                                <div>Peso: {formatWeightSpanish(pokemon?.weight).toFixed(2)} kgs.</div>

                        </main>
                </div>
                <Footer></Footer>
                
                
        </>        )
        
};


export default Pokemon;
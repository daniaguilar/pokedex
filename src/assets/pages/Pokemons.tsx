
import {useEffect ,useState} from "react";
import  { Link } from "react-router-dom";

import  Header  from "../components/Header";
import  Footer  from "../components/Footer";

//import BulbsaurPic from "../icons/bulbasaur.gif"
import styles from "../pages/pokemons.module.css";
import { fetchPokemons } from "../../api/fetchPokemons";
import { Pokemon } from "../../types/types";
import LoadingScreen from "../components/LoadingScreen";
import { waitFor } from "../../utils/utils";


const Pokemons = ()=> {
    const [isLoading,setIsLoading] = useState(false); //? Parametro que iniciación para la la pantallade carga.
    const [query,setQuery] = useState(""); //? Parametro que recoge del buscador el parámetro.
    const [pokemons,setPokemons] = useState<Pokemon[]>([]);

    useEffect(() => {
        setIsLoading(true);
        waitFor(1000);
        const fetchAllPokemons = async () => {
            const allPokemons = await fetchPokemons();
            //console.log(allPokemons);
            setPokemons(allPokemons);
            setIsLoading(false);
        };
        fetchAllPokemons();
    
    },[])

    //? Se crea la llamada a la pantalla de carga.
    if (isLoading || !pokemons) {
        return <LoadingScreen/>
    }

    const filterPokemons =pokemons?.slice(0,649).filter((pokemon) => {
        return pokemon.name.toLowerCase().match(query.toLowerCase())
})

    return (
        <>
        <Header query={query} setQuery={setQuery}/>
        <main>

            <nav className={styles.nav}>
                {filterPokemons?.slice(0,649).map((pokemon: any)=> (
                    <Link 
                    to= {`/pokemons/${pokemon.name.toLowerCase()}`}
                    className={styles.listItem}
                    key={pokemon.id}
                >
                    <img src={pokemon.imgSrc} alt={pokemon.name} className={styles.listItemIcon}/>
                <div className={styles.listItemText}>
                    <span>{pokemon.name}</span>
                    <span>{pokemon.id}</span>
                </div>
                </Link>
                ))}
                
            </nav>

        </main>
        <Footer/>
        </>
    );
};


export default Pokemons; 
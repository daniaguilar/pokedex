import { Link } from "react-router-dom"
import styles from "./footer.module.css"
//Assetss 
import PokemonPic from "../icons/pikachu.png"
import LocationPic from "../icons/pointer.png"
import ItemsPic from "../icons/pokeball.svg"
const Footer = () =>{
    return <footer className={styles.footer}>
    <div className={styles.linkContainer}>
        <Link className={styles.footerLink} to="/pokemons">
            <img className={styles.icon} src={PokemonPic} alt="Pokeball" />
            Pokemons
        </Link>
        <Link className={styles.footerLink} to="/items">
            <img className={styles.icon} src={ItemsPic} alt="Pokeball" />
            Items
        </Link>
        <Link className={styles.footerLink} to="/location">
            <img className={styles.icon} src={LocationPic} alt="Pokeball" />
            Mapa
        </Link>
    </div>
</footer>
}

export default Footer;
import React from "react"
import Pokedex from "../icons/pokedex.png"
import styles from "./loadingScreen.module.css";
const LoadingScreen = () =>{
    return (
        <div
            className={styles.loadingScreen}
        >
            <img 
            src={Pokedex} 
            alt="Pokedex"
            className={styles.loadingScreenItem}
            />
            <span
                className={styles.loadingScreemSpan}
            >Cargando...</span>
                
        </div>
    )
}

export default  LoadingScreen;
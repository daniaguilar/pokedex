import  styles from "../components/header.module.css";
//? Se debe definir que tipo de variables recibe (typescript) 
type HeadersProps = {
    query: string;
    setQuery: (query: string) =>void;

}

const Header = ({query,setQuery}:HeadersProps) =>{
        return(
            <header className={styles.header}>
                <input type='text' 
                className={styles.input} 
                placeholder="Busca un Pokémon"
                value ={query}
                onChange={(e)=> setQuery(e.target.value)}
                
                />
            </header>
        )
}


export default Header;
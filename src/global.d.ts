declare module "*.module.css" {
    const classes: { [key:string]:string };
    export default classes;
}

//! Importante esta declaración pora que se reconozcan los estilos, por  los componentes.

declare module "*.png";
declare module "*.svg";
declare module "*.gif";

import { resolve } from "path";

export function formatPokemonName(name: string): string {
    //? Si es hembra se sustituye por -f.
    if (name.includes("♀")) {
        return name.replace ("♀","-f");
    //? Si es macho se sustituye por -m.
    } else if (name.includes("♂")){
        return name.replace ("♂","-m");
    //? Si hay "." se sustituye por "-".
    } else if (name.includes(".")){
        return name.replace (".", "-")
    //? Si es "farfetch'd" se sustituye por "farfetchd".
    }else if (name.includes("farfetch'd")){
        return name.replace ("farfetch'd","farfetchd")
     //? Para el resto de casos.
    } else return name;

}

export function waitFor (time: number): Promise<void> {
    return new Promise((resolve) => setTimeout(resolve, time));
}


export function formatHeightSpanish(height: number): number {
    height = (height * 0.1)
    return height
    
}

export function formatWeightSpanish(weight: number): number {
    weight = weight/10
    return weight 
}




export function formatTypeSpanish(type: string): any {
    
    switch (type){
    case 'steel':
        return type="Acero"
    
    case 'water':
            return type="Agua"
    
    case 'bug':
        return type="Bicho"
    
    case 'dragon':
        return type="Dragón"    
    
    case 'electric':
        return type="Eléctrico"
    
    case 'ghost':
        return type="Fantasma"
    
    case 'fire':
            return type="Fuego"
            
    case 'fairy':
        return type="Hada"
    
    case 'ice':
        return type="Hielo"
    
    case 'fighting':
        return type="Lucha"

    case 'normal':
        return type= "Normal"
    
    case 'glass':
        return type = "Planta"
    
    case 'psychic':
        return type = "Psíquico"
    
    case 'rock':
        return type = "Roca"
    
    case 'dark':
        return type = "Siniestro"

    case 'ground':
        return type = "Tierra"

    case 'poison':
        return type = "Venenoso"
    
    case 'flying':
        return type = "Volador"
    
    default:
        return type = "No definido"

    }
}




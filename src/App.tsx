import'./App.css';
import {BrowserRouter as Router, Routes,Route} from 'react-router-dom';
import {Items,Pokemons,Pokemon} from './assets/pages'

function App() {
  return (
    <Router>
        <div className="app">
          <Routes>
              <Route path="/pokemons/:name"  element={<Pokemon />}/>
              <Route path="/pokemons"  element={<Pokemons />}/>
              <Route path="/"  element= {<Pokemons />}/>
              <Route path="/items"  element= {<Items />}/>
          </Routes>
        </div>
 

    </Router>
        

  );
}

export default App;
